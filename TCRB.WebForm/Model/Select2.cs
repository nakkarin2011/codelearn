﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TCRB.WebForm.Model
{
    public class Select2Model
    {
        public string id { get; set; }
        public string text { get; set; }
        public bool selected { get; set; }
    }

    public class Select2SearchModel
    {
        public string Search { get; set; }
        public int Page { get; set; }
    }
}