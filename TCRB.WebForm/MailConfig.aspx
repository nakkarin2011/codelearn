﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="MailConfig.aspx.cs" Inherits="TCRB.WebForm.MailConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="ViewMail"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="serverScript" runat="server">
     <script type="text/javascript">
         $(document).ready(function () {

             _Ajax("/TCRB/MailSetup/Index",{}
                    , null, function (document) {
                        $('#ViewMail').html(document);
                    });
         });

    </script>
</asp:Content>
