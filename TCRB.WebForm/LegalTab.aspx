﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="LegalTab.aspx.cs" Inherits="TCRB.WebForm.LegalTab" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="ViewLegal"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="serverScript" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            _Ajax("/TCRB/LegalInfo/Index", {}
                , null, function (document) {
                    $('#ViewLegal').html(document);
                });
        });


    </script>
</asp:Content>
