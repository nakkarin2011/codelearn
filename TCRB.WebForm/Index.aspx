﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="TCRB.WebForm.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Content Header (Page header) -->
    <div id="frmSales" class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">SalesReason</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">SalesReason</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
          <div class="row">
              <div class="col-md-12">
           
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Search Form</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                    <div class="card-body">
                        <form id="formSearch" class="form-horizontal">
                            <div class="form-group row">
                                <label for="inputEmail3"  class="col-form-label">Sales Reason Name: </label>
                                <div class="col-sm-4">
                                    <input  v-model="strReasonName"  type="text" class="form-control" placeholder="Sales Reason Name..">
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-info" v-on:click="SearchData();"><i class="fa fa-search"></i> Search</button>
                                    <button class="btn btn-default" type="button" v-on:click="ResetData"><i class="fa fa-sync-alt"></i> Reset</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                 <label for="input3"  class="col-form-label">Sales Reason Name: </label>
                                <div class="col-sm-4">
                                <select class="form-control">
                                    <option value="">Choose</option>
                                    <option v-for="option in ListSalesReason" v-bind:value="option.code">{{ option.name }}
                                    </option>
                                </select>
                                    </div>
                            </div>
                        </form>
                        <div class="form-group row">
                             <button type="button" class="btn btn-primary" v-on:click="addData"><i class="fa fa-plus"></i> Add</button>
                        </div>
                        <div class="form-group row">
                            <table id="tableSearch" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>SalesReasonKey</th>
                                        <th>SalesReasonAlternateKey</th>
                                        <th>SalesReasonName</th>
                                        <th>SalesReasonReasonType</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    
                    <!-- /.card-footer -->
              
            </div>
            <!-- /.card -->

            </div>
          </div>
          
      </div><!-- /.container-fluid -->
         <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">{{modalTitle}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p v-if="errors.length">
                    <b class="text-danger">Please correct the following error(s):</b>
                    <ul>
                        <li v-for="error in errors" class="text-danger">{{ error }}</li>
                    </ul>
                </p>
                <div class="form-group">
                    <label for="exampleInputEmail1">SalesReason Name</label>
                    <input type="email" v-model="SalesReasonName" class="form-control" id="srName" placeholder="Enter SalesReason Name">
                </div>
                 <div class="form-group">
                    <label for="exampleInputEmail1">SalesReasonReason Type</label>
                    <input type="email" v-model="SalesReasonReasonType" class="form-control" id="srType" placeholder="Enter SalesReasonReason Type">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary"  v-on:click="submitFormData();">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </div>
    <!-- /.content-header -->
  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="serverScript" runat="server">
    <script type="text/javascript">
        var tableSearch;
        var app = new Vue({
            el: '#frmSales',
            data: {
                modalTitle: '',
                status: '',
                SalesReasonKey: '',
                SalesReasonAlternateKey: '',
                SalesReasonName: '',
                SalesReasonReasonType: '',
                strReasonName: '',
                errors: [],
                ListSalesReason: []

            },
            methods: {
                ResetData: function () {
                    app.SalesReasonKey = '';
                    app.SalesReasonAlternateKey = '';
                    app.SalesReasonName = '';
                    app.SalesReasonReasonType = '';
                    app.strReasonName = '';
                    app.errors = [];
                },
                SearchData: function () {
                    tableSearch.fnDraw();
                },
                addData: function () {
                    this.modalTitle = "Add Form";
                    this.status = 'Add';
                    this.ResetData();

                    $("#modal-default").modal('show');
                },
                editData: function (id) {
                    this.modalTitle = "Edit Form";
                    this.status = 'Edit';
                    this.errors = [];

                    $.ajax({
                        type: "POST",
                        url: "../Controllers/wsService.asmx/GetDataBySalesReasonKey",
                        data: "{'SalesReasonKey':" + id + "}",
                        contentType: "application/json",
                        datatype: "json",
                        success: function (res) {
                            var obj = JSON.parse(res.d);
                            app.SalesReasonKey = obj.SalesReasonKey;
                            app.SalesReasonAlternateKey = obj.SalesReasonAlternateKey;
                            app.SalesReasonName = obj.SalesReasonName;
                            app.SalesReasonReasonType = obj.SalesReasonReasonType;
                            console.log(obj);
                        }
                    });

                    $("#modal-default").modal('show');
                },
                deleteData: function (id) {
                    var r = confirm("Are you sure to delete this item?");
                    if (r == true) {
                        $.ajax({
                            type: "POST",
                            url: "../Controllers/wsService.asmx/DeleteDataSaleReason",
                            data: "{'SalesReasonKey':" + id + "}",
                            contentType: "application/json",
                            datatype: "json",
                            success: function (res) {
                                console.log(res.d);
                                toastr.error(res.d);
                                tableSearch.fnDraw();
                            }
                        });
                    }
                },
                submitFormData: function () {
                    let self = this;

                    if (app.validateForm()) {
                        $.ajax({
                            type: "POST",
                            url: "../Controllers/wsService.asmx/SaveChangeData",
                            data: JSON.stringify({
                                SalesReasonKey: self.SalesReasonKey,
                                SalesReasonAlternateKey: self.SalesReasonAlternateKey,
                                SalesReasonName: self.SalesReasonName,
                                SalesReasonReasonType: self.SalesReasonReasonType,
                                status: self.status
                            }),
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (res) {
                                console.log(res.d);
                                toastr.success(res.d);
                                app.ResetData();
                                $("#modal-default").modal('hide');
                                tableSearch.fnDraw();
                            }
                        });
                    }
                },
                validateForm: function () {
                    app.errors = [];
                    if (app.SalesReasonName === '') {
                        app.errors.push("Sales Reason Name is required.");
                    }
                    if (app.SalesReasonReasonType === '') {
                        app.errors.push("Sales Reason Reason Type is required.");

                    }
                    if (!app.errors.length) {
                        return true
                    }
                    else {
                        return false
                    }
                },
                InquiryListSalesReason: function () {
                    let seft = this;

                    $.ajax({
                        type: "POST",
                        url: "../Controllers/wsService.asmx/GetListSalesReason",
                        contentType: "application/json",
                        datatype: "json",
                        success: function (res) {
                            var obj = JSON.parse(res.d);
                            console.log(obj);
                            seft.ListSalesReason = obj;

                        }
                    });
                },
                selectedOption: function (option) {
                    console.log(option);

                    if (this.value) {
                        return option.code === this.value.code;
                    }
                    return false;
                }
                
            },
            mounted() {
                let self = this;
                self.InquiryListSalesReason();

                tableSearch = $("#tableSearch").dataTable({
                    lengthChange: false,
                    processing: false,
                    searching: false,
                    pageLength: 5,
                    serverSide: true,
                    responsive: true,
                    ajax: {
                        contentType: "application/json; charset=utf-8",
                        url: '../Controllers/wsService.asmx/InquerySalesReason',
                        type: "POST",
                        //dataType: "json",
                        data: function (d) {
                            console.log(d);

                            return JSON.stringify({
                                option: d, strReasonName: self.strReasonName
                            });
                            //return $formSearch.serializeArray().reduce(function (a, x) {
                            //    console.log(a);
                            //    d[x.name] = x.value;
                            //    return d;
                            //}, {});
                        },
                        dataFilter: function (res) {
                            // You probably know by know that WebServices
                            // puts your data in an object call d. these two lines puts it
                            // in the form that DataTable expects
                            var parsed = JSON.parse(res);
                            console.log(parsed.d);
                            return JSON.stringify(parsed.d);
                        },
                        //contentType: "application/json",
                        beforeSend: function (d) {
                            console.log(d);
                        },
                        complete: function (d) {
                            console.log(d);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(thrownError);
                            //if (xhr.status == 401) { $(location).attr('href', '@Url.Action("SessionExpired", "Account")'); }
                        }
                        //dataSrc: "",
                    },
                    columns: [
                        { data: "SalesReasonKey" },
                        { data: "SalesReasonAlternateKey" },
                        { data: "SalesReasonName", },
                        { data: "SalesReasonReasonType" },
                        {
                            data: null,
                            render: function (o) {
                                return '<a id="edit" class="btn btn-block btn-primary" href="javascript:;" data-id="' + o.SalesReasonKey + '"><i class="fa fa-edit"></i></a>';

                            }
                        },
                        {
                            data: null,
                            render: function (o) {
                                return '<a id="delete" class="btn btn-block btn-danger" href="javascript:;" data-id="' + o.SalesReasonKey + '"><i class="fa fa-trash-alt"></i></a>';
                            }
                        },
                    ],
                    order: [
                        [1, "asc"]
                    ],

                }).on('click', '#edit', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    //alert('Edit id=' + dataRow.SalesReasonKey);
                    //tableSearch.fnDraw();
                    app.editData(dataRow.SalesReasonKey);
                }).on('click', '#delete', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    app.deleteData(dataRow.SalesReasonKey);
                });
            },
        });
</script>
</asp:Content>
