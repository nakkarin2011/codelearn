﻿using KTBCONRMS.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TCRB.BAL;
using TCRB.DAL;
using TCRB.Model;
using TCRB.WebForm.Model;

namespace TCRB.WebForm.Controllers
{
    public class MailSetupController : Controller
    {
        // GET: MailSetup
        public ActionResult Index()
        {
            List<Select2Model> Master_Select2Single = new List<Select2Model>();
            using (MailTypeRep rep = new MailTypeRep())
            {
                var query = (from a in rep.GetAll()
                             select new Select2Model
                             {
                                 id = a.Mail_Type.ToString(),
                                 text = a.Mail_Type_Desc
                             });

                Master_Select2Single = query.ToList();
            }
        
  
            var result = new ViewIndex()
            {
                Master_Select2Single = Master_Select2Single,
                
            };

            return View(result);
        }

        [HttpPost]
        public JsonResult ListMailType()
        {
            try
            {
                using (MailTypeRep rep = new MailTypeRep())
                {
                    var query = (from a in rep.GetAll()
                                 select new System.Web.Mvc.SelectListItem
                                 {
                                     Text = a.Mail_Type_Desc,
                                     Value = a.Mail_Type.ToString()
                                 });

                    return Json(query);
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult InquiryData(DatableOption option, msMailConfig model)
        {
            using (MailConfigRep rep = new MailConfigRep())
            {
                using (MailTypeRep rep2 = new MailTypeRep())
                {
                    var query = (from a in rep.GetAll()
                                 join b in rep2.GetAll() on a.Mail_Type equals b.Mail_Type
                                 where (string.IsNullOrEmpty(model.Mail_Subject) || a.Mail_Subject.ToUpper().Contains(model.Mail_Subject.ToUpper()))
                                 && (model.Mail_Type == null || a.Mail_Type == model.Mail_Type)
                                 select new
                                 {
                                     Mail_ID = a.Mail_ID,
                                     Mail_Type = a.Mail_Type,
                                     Mail_SMTP = a.Mail_SMTP,
                                     Mail_From = a.Mail_From,
                                     Mail_To = a.Mail_To,
                                     Mail_CC1 = a.Mail_CC1,
                                     Mail_CC2 = a.Mail_CC2,
                                     Mail_Subject = a.Mail_Subject,
                                     Mail_Body = a.Mail_Body,
                                     Mail_Type_Desc = b.Mail_Type_Desc
                                 });

                    switch (option.sortingby)
                    {
                        case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.Mail_Type_Desc) : query.OrderByDescending(r => r.Mail_Type_Desc)); break;
                        case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.Mail_From) : query.OrderByDescending(r => r.Mail_From)); break;
                        case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.Mail_To) : query.OrderByDescending(r => r.Mail_To)); break;
                        case 4: query = (option.orderby == "asc" ? query.OrderBy(r => r.Mail_CC1) : query.OrderByDescending(r => r.Mail_CC1)); break;
                        case 5: query = (option.orderby == "asc" ? query.OrderBy(r => r.Mail_CC2) : query.OrderByDescending(r => r.Mail_CC2)); break;
                        case 6: query = (option.orderby == "asc" ? query.OrderBy(r => r.Mail_Subject) : query.OrderByDescending(r => r.Mail_Subject)); break;
                        default: query = (option.orderby == "asc" ? query.OrderBy(r => r.Mail_Type_Desc) : query.OrderByDescending(r => r.Mail_Type_Desc)); break;
                    }


                    var datas = query.Skip(option.start).Take(option.length).ToList();
                    var recordsTotal = query.Count();

                    var result = new JsonDataTable
                    {
                        status = true,
                        message = "success",
                        data = datas,
                        draw = option.draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsTotal
                    };

                    return Json(result);
                }
            }
        }

        [HttpPost]
        public JsonResult InquiryDataListEmployee(DatableOption option, string name)
        {
            using (AdventureWorksDW2017Entities _context = new AdventureWorksDW2017Entities())
            {
                var query = (from a in _context.vlistCustomer.ToList()
                             where (string.IsNullOrEmpty(name) || a.FirstName.ToUpper().Contains(name.ToUpper()))
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.CustomerKey) : query.OrderByDescending(r => r.CustomerKey)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.FirstName) : query.OrderByDescending(r => r.FirstName)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.EmailAddress) : query.OrderByDescending(r => r.EmailAddress)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.CustomerKey) : query.OrderByDescending(r => r.CustomerKey)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }

        [HttpPost]
        public JsonResult SaveChangeData(msMailConfig model, string status)
        {
            var Result = string.Empty;

            try
            {

                using (MailConfigRep rep = new MailConfigRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(model);
                                Result = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(model);
                                Result = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }


            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteData(msMailConfig model)
        {
            var Result = string.Empty;

            try
            {
                using (MailConfigRep rep = new MailConfigRep())
                {
                    rep.DeleteData(model);
                    Result = "Delete Data Success!!";

                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }
    }
}