﻿using KTBCONRMS.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TCRB.DAL;

namespace TCRB.WebForm.Controllers
{
    public class LegalInfoController : Controller
    {
        // GET: LegalInfo

       public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult InquiryData(DatableOption option, string FirstName)
        {
            using (AdventureWorksDW2017Entities _context = new AdventureWorksDW2017Entities())
            {
                var query = (from a in _context.vlistCustomer.ToList()
                             where (string.IsNullOrEmpty(FirstName) || a.FirstName.Contains(FirstName))
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.CustomerKey) : query.OrderByDescending(r => r.CustomerKey)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.FirstName) : query.OrderByDescending(r => r.FirstName)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.EmailAddress) : query.OrderByDescending(r => r.EmailAddress)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.CustomerKey) : query.OrderByDescending(r => r.CustomerKey)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }
    }
}