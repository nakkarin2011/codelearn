﻿using KTBCONRMS.Model.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using TCRB.BAL;
using TCRB.Model;

namespace TCRB.WebForm.Controllers
{
    /// <summary>
    /// Summary description for wsService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class wsService : System.Web.Services.WebService
    {

        [WebMethod]
        public JsonDataTable InquerySalesReason(DatableOption option, string strReasonName)
        {
      
            using (SalesReasonRep rep = new SalesReasonRep())
            {
                var query = (from a in rep.GetAll()
                             where string.IsNullOrEmpty(strReasonName) || a.SalesReasonName.ToUpper().Contains(strReasonName.ToUpper().Trim())
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.SalesReasonKey) : query.OrderByDescending(r => r.SalesReasonKey)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.SalesReasonAlternateKey) : query.OrderByDescending(r => r.SalesReasonAlternateKey)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.SalesReasonName) : query.OrderByDescending(r => r.SalesReasonName)); break;
                    case 4: query = (option.orderby == "asc" ? query.OrderBy(r => r.SalesReasonReasonType) : query.OrderByDescending(r => r.SalesReasonReasonType)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.SalesReasonKey) : query.OrderByDescending(r => r.SalesReasonKey)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            } 
        }

        [WebMethod]
        public string GetDataBySalesReasonKey(string SalesReasonKey)
        {
            string strResult = string.Empty;

            using (SalesReasonRep rep = new SalesReasonRep())
            {
                var query = (from a in rep.GetAll()
                             where a.SalesReasonKey == Convert.ToInt32(SalesReasonKey.Trim())
                             select a).FirstOrDefault();

                if (query != null)
                {
                    strResult = JsonConvert.SerializeObject(query);
                }
            }
            return strResult;   
        }
        [WebMethod]
        public string DeleteDataSaleReason(string SalesReasonKey)
        {
            string strResult = string.Empty;
            try
            {
                using (SalesReasonRep rep = new SalesReasonRep())
                {
                    rep.DeleteData(new DimSalesReason
                    {
                        SalesReasonKey = Convert.ToInt32(SalesReasonKey.Trim())
                    });
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult; 
        }
        [WebMethod]
        public string SaveChangeData(string SalesReasonKey
                                    , string SalesReasonAlternateKey
                                    , string SalesReasonName
                                    , string SalesReasonReasonType
                                    , string status)

        {
            string strResult = string.Empty;
            try
            {
                using (SalesReasonRep rep = new SalesReasonRep())
                {
                    switch (status)
                    {
                        case "Add":
                            rep.InsertData(new DimSalesReason
                            {
                                SalesReasonName = SalesReasonName,
                                SalesReasonReasonType = SalesReasonReasonType
                            });
                            strResult = "Insert Data Success!!";
                            break;
                        case "Edit":
                            rep.UpdateData(new DimSalesReason
                            {
                                SalesReasonKey = Convert.ToInt32(SalesReasonKey),
                                SalesReasonAlternateKey = Convert.ToInt32(SalesReasonAlternateKey),
                                SalesReasonName = SalesReasonName,
                                SalesReasonReasonType = SalesReasonReasonType
                            });
                            strResult = "Update Data Success!!";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetListSalesReason()
        {

            using (SalesReasonRep rep = new SalesReasonRep())
            {
                var query = (from a in rep.GetAll()
                             select new SelectOptionModels
                             {
                                 name = a.SalesReasonName,
                                 code = a.SalesReasonKey.ToString()
                             }).ToList();

                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(query);

            }
        }
    }
}
