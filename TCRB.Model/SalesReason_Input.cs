﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCRB.Model
{
   public class SalesReason_Input
    {
        public string SalesReasonKey { get; set; }
        public string SalesReasonAlternateKey { get; set; }
        public string SalesReasonName { get; set; }
        public string SalesReasonReasonType { get; set; }
    }
}
