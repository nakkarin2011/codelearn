﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCRB.BAL
{
    public interface IRespository<T>
    {
        void InsertData(T model);
        void UpdateData(T model);
        void DeleteData(T model);
        List<T> GetAll();
        List<T> Find(T model);
    }
}
