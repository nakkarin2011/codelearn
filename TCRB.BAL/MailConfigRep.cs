﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCRB.DAL;
using TCRB.Model;

namespace TCRB.BAL
{
    public class MailConfigRep : IRespository<msMailConfig>, IDisposable
    {
        public readonly AdventureWorksDW2017Entities _context;

        public MailConfigRep()
        {
            _context = new AdventureWorksDW2017Entities();
        }
        public void DeleteData(msMailConfig model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<msMailConfig> Find(msMailConfig model)
        {
            try
            {
                return _context.msMailConfig.Where(a => a.Mail_ID == model.Mail_ID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<msMailConfig> GetAll()
        {
            return _context.msMailConfig.ToList();
        }

        public void InsertData(msMailConfig model)
        {
            try
            {
                _context.msMailConfig.Add(model);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateData(msMailConfig model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
