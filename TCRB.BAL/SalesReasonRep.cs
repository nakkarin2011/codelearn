﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCRB.DAL;
using TCRB.Model;

namespace TCRB.BAL
{
    public class SalesReasonRep : IRespository<DimSalesReason>,IDisposable
    {
        public readonly AdventureWorksDW2017Entities _context;

        public SalesReasonRep()
        {
            _context = new AdventureWorksDW2017Entities();
        }

        public void DeleteData(DimSalesReason model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<DimSalesReason> Find(DimSalesReason model)
        {
            try
            {
                return _context.DimSalesReason.Where(a => a.SalesReasonName.Contains(model.SalesReasonName.Trim())).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<DimSalesReason> GetAll()
        {
            try
            {
                var _result = from a in _context.DimSalesReason.ToList()
                              select new DimSalesReason()
                              {
                                  SalesReasonKey = a.SalesReasonKey,
                                  SalesReasonAlternateKey = a.SalesReasonAlternateKey,
                                  SalesReasonName = a.SalesReasonName,
                                  SalesReasonReasonType = a.SalesReasonReasonType
                              };
                return _result.ToList();
            }
            catch
            {
                throw;
            }
        }

        public void InsertData(DimSalesReason model)
        {
            try
            {
                _context.DimSalesReason.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void UpdateData(DimSalesReason model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
