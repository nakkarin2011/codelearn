﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCRB.DAL;
using TCRB.Model;

namespace TCRB.BAL
{
    public class MailTypeRep : IRespository<msMailType>, IDisposable
    {
        public readonly AdventureWorksDW2017Entities _context;

        public MailTypeRep()
        {
            _context = new AdventureWorksDW2017Entities();
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msMailType model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msMailType> Find(msMailType model)
        {
            try
            {
                return _context.msMailType.Where(a => a.Mail_Type == model.Mail_Type).ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<msMailType> GetAll()
        {
            return _context.msMailType.ToList();
        }

        public void InsertData(msMailType model)
        {
            try
            {
                _context.msMailType.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void UpdateData(msMailType model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
